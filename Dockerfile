# Copyright 2016 The Kubernetes Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
FROM arm32v6/alpine

RUN apk add --no-cache gcc musl-dev
RUN mkdir -p /pause-src
WORKDIR /pause-src
ADD orphan.c pause.c /pause-src/
RUN gcc -Os -Wall -Werror -static  pause.c -o pause

FROM scratch
COPY --from=0 /pause-src/pause /pause
ENTRYPOINT ["/pause"]
