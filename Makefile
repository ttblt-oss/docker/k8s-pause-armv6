
# https://github.com/kubernetes/kubernetes/tree/master/build/pause
build:
	gcc -Os -Wall -Werror -static  pause.c -o pause

docker:
	docker build -t registry.gitlab.com/ttblt-hass/docker-images/k8s-pause-armv6:latest .

fake-k8s-pause:
	# Useful only with docker
	docker pull registry.gitlab.com/ttblt-hass/docker-images/k8s-pause-armv6:latest
	docker tag registry.gitlab.com/ttblt-hass/docker-images/k8s-pause-armv6:latest k8s.gcr.io/pause:3.1
